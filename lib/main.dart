import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:helloworld/Secondpage.dart';
import 'package:get/get.dart';
import 'package:helloworld/Controller.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

void main() {
  runApp(Root());
}

class Root extends StatelessWidget {
  const Root({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  Controller controller = Get.put(Controller()); // Rather Controller controller = Controller();
  String _string = 'All';
  @override
  Widget build(BuildContext context) {
    return (Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Task Manager"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              child: Obx(() => DropdownButtonHideUnderline(
                    child: DropdownButton(
                        value: controller.dropDownItem.value,
                        items: [
                          DropdownMenuItem(
                            child: Text("All"),
                            value: 'All',
                          ),
                          DropdownMenuItem(
                            child: Text("Todo"),
                            value: 'Todo',
                          ),
                          DropdownMenuItem(
                            child: Text("Doing"),
                            value: 'Doing',
                          ),
                          DropdownMenuItem(
                            child: Text("Done"),
                            value: 'Done',
                          ),
                        ],
                        onChanged: (String? value) {
                          controller.dropDownItem.value = value!;
                          controller.filterTasks();
                        },
                        hint: Text("Select item")),
                  )),
            ),
            Expanded(child: DynamicListView(controller))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            print("Clicked");
            //Navigator.push(context, new MaterialPageRoute(builder: (context) => new SecondPage()));
            Get.to(SecondPage());
          },
          child: const Icon(Icons.add),
          backgroundColor: Colors.green,
        )));
  }
}

class DynamicListView extends StatelessWidget {
  Controller controller;
  DynamicListView(this.controller);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: <Widget>[
      Expanded(
          child: Obx(() => ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: controller.Tasks.length,
              itemBuilder: (BuildContext context, int index) {
                return Slidable(
                    key: const ValueKey(0),

                    // The start action pane is the one at the left or the top side.
                    startActionPane: ActionPane(
                      // A motion is a widget used to control how the pane animates.
                      motion: ScrollMotion(),

                      // A pane can dismiss the Slidable.
                      dismissible: DismissiblePane(onDismissed: () {}),

                      // All actions are defined in the children parameter.
                      children: [
                        // A SlidableAction can have an icon and/or a label.
                        SlidableAction(
                          onPressed: (context) {
                            controller.deleteTask(controller.Tasks[index]);
                          },
                          backgroundColor: Color(0xFFFE4A49),
                          foregroundColor: Colors.white,
                          icon: Icons.delete,
                          label: 'Delete',
                        ),
                      ],
                    ),
                    child: ListTile(trailing: Text('${controller.Tasks[index].Status}'), title: Text('${controller.Tasks[index].Title}', style: TextStyle(fontSize: 18))));
              })))
    ]));
  }
}

class Task {
  String Title = '';
  String Description = '';
  String Status = '';
}
