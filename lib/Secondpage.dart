import 'package:flutter/material.dart';
import 'package:helloworld/main.dart';
import 'package:helloworld/Controller.dart';
import 'package:get/get.dart';

// lib: slidable
class SecondRoute extends State<SecondPage> {
  String _string = 'Todo';
  String _title = '';
  String _description = '';
  Controller controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Back"),
        actions: [
          ElevatedButton(
            onPressed: () {
              Task T1 = Task();
              T1.Title = _title;
              T1.Description = _description;
              T1.Status = _string;
              controller.AddTask(T1);
              Get.back();
            },
            child: Text('Save'),
          )
        ],
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          padding: EdgeInsets.all(20),
          child: DropdownButton(
              value: _string,
              items: [
                DropdownMenuItem(
                  child: Text("Todo"),
                  value: 'Todo',
                ),
                DropdownMenuItem(
                  child: Text("Doing"),
                  value: 'Doing',
                ),
                DropdownMenuItem(
                  child: Text("Done"),
                  value: 'Done',
                ),
              ],
              onChanged: (String? value) {
                setState(() {
                  _string = value!;
                });
              },
              hint: Text("Select item")),
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Task Title',
              hintText: 'Enter task title',
            ),
            onChanged: (Value) {
              setState(() {
                _title = Value;
              });
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            maxLines: 7,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Task Description',
              hintText: 'Describe your task',
            ),
            onChanged: (Value) {
              setState(() {
                _description = Value;
              });
            },
          ),
        ),
      ]),
    );
  }
}

class SecondPage extends StatefulWidget {
  @override
  State<SecondPage> createState() => SecondRoute();
}
