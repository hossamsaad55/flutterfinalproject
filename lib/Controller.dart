import 'package:get/get.dart';
import 'package:helloworld/main.dart';

class Controller extends GetxController {
  var Tasks = [].obs;
  var allTasks = [].obs;
  var dropDownItem = 'All'.obs;
  @override
  void onInit() {
    super.onInit();
    Task T1 = Task();
    T1.Description = 'call friend';
    T1.Title = 'Phone calls';
    T1.Status = 'Todo';
    Task T2 = Task();
    T2.Description = 'e-mail my manager';
    T2.Title = 'Send e-mails';
    T2.Status = 'Doing';
    Task T3 = Task();
    T3.Description = 'Go to the gym';
    T3.Title = 'Sports';
    T3.Status = 'Done';
    allTasks.add(T1);
    allTasks.add(T2);
    allTasks.add(T3);
    filterTasks();
  }

  void filterTasks() {
    Tasks.clear();
    if (dropDownItem.value == 'All') {
      Tasks.addAll(allTasks);
    } else {
      Tasks = allTasks.where((i) => i.Status == dropDownItem.value).toList().obs;
    }
  }

  void AddTask(Task task) {
    allTasks.add(task);
    filterTasks();
  }

  void deleteTask(Task task) {
    Tasks.remove(task);
    filterTasks();
  }
}
